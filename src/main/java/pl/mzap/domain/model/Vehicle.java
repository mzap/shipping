package pl.mzap.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.status.VehicleStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Map;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	private String plate;
	private String driver;
	private VehicleKind vehicleKind;
	//todo: use linkedHashMap to order
	private Map<LocalDateTime, VehicleLocationHistory> status;

}
