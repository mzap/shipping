package pl.mzap.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.domain.kind.Relation;
import pl.mzap.domain.status.OrderStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	@OneToOne
	private Contractor contractor;
	private Relation relation;
	private String referenceNumber;
	private double paymentAmount;
	private int paymentTermInDays;
	private List<Cargo> cargos;
	private List<Transport> transports;
	@OneToOne //todo: use linkedHashMap to order
	private Map<LocalDateTime, OrderStatus> status;

}
