package pl.mzap.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.domain.status.ContractorStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contractor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	private String name;
	private String shortName;
	private String vatId;
	private String phoneNumber;
	@OneToOne
	private Address address;
	private Map<LocalDateTime, ContractorStatus> status;

}
