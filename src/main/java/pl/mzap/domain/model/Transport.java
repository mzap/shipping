package pl.mzap.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.domain.kind.TransportAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    private TransportAction transportAction;
    private String referenceNumber;
    private String companyName;
    private LocalDateTime date;
    @OneToOne //todo: use id's and join column to identifies object by id
    private Cargo cargo;
    @OneToOne
    private Vehicle vehicle;
    @OneToOne
    private Address address;

}
