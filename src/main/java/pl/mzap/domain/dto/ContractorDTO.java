package pl.mzap.domain.dto;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableStringValue;
import lombok.*;
import pl.mzap.domain.model.Address;
import pl.mzap.domain.status.ContractorStatus;

import java.util.Observable;
import java.util.Observer;

@Builder
public class ContractorDTO {

	private StringProperty name;
	private StringProperty shortName;
	private StringProperty phoneNumber;
	private StringProperty vatId;
	private ObjectProperty<AddressDTO> address;
	private ObjectProperty<ContractorStatus> status;

	public String getName() {
		return name.get();
	}
	public String getShortName() {
		return shortName.get();
	}
	public String getPhoneNumber() {
		return phoneNumber.get();
	}
	public String getVatId() {
		return vatId.get();
	}
	public AddressDTO getAddress() {
		return address.get();
	}
	public ContractorStatus getStatus() {
		return status.get();
	}

	public StringProperty nameProperty() {
		return name;
	}
	public StringProperty shortNameProperty() {
		return shortName;
	}
	public StringProperty phoneNumberProperty() {
		return phoneNumber;
	}
	public StringProperty vatIdProperty() {
		return vatId;
	}

	public void setName(String name) {
		this.name = new SimpleStringProperty(name);
	}
	public void setShortName(String shortName) {
		this.shortName = new SimpleStringProperty(shortName);
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = new SimpleStringProperty(phoneNumber);
	}
	public void setVatId(String vatId) {
		this.vatId = new SimpleStringProperty(vatId);
	}
	public void setAddress(AddressDTO address) {
		this.address = new SimpleObjectProperty<>(address);
	}
	public void setStatus(ContractorStatus status) {
		this.status = new SimpleObjectProperty<>(status);
	}

	@Override
	public String toString() {
		return name.get() + " [" + shortName.get() + "]";
	}
}
