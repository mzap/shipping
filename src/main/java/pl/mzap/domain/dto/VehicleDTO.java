package pl.mzap.domain.dto;

import javafx.beans.property.*;
import javafx.collections.ObservableMap;
import lombok.*;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.status.VehicleStatus;

import java.time.LocalDateTime;
import java.util.Optional;

@Builder
public class VehicleDTO {

	private StringProperty plate;
	private StringProperty driver;
	private ObjectProperty<VehicleKind> vehicleKind;
	private MapProperty<LocalDateTime, VehicleLocationHistoryDTO> status;

	public String getPlate() {
		return plate.get();
	}
	public String getDriver() {
		return driver.get();
	}
	public VehicleKind getVehicleKind() {
		return vehicleKind.get();
	}
	public ObservableMap<LocalDateTime, VehicleLocationHistoryDTO> getStatus() {
		return status.get();
	}

	public void setPlate(String plate) {
		this.plate = new SimpleStringProperty(plate);
	}
	public void setDriver(String driver) {
		this.driver = new SimpleStringProperty(driver);
	}
	public void setVehicleKind(VehicleKind vehicleKind) {
		this.vehicleKind = new SimpleObjectProperty<>(vehicleKind);
	}
	public void setStatus(ObservableMap<LocalDateTime, VehicleLocationHistoryDTO> status) {
		this.status = new SimpleMapProperty<>(status);
	}
	public void addStatus(VehicleStatus vehicleStatus) {
		System.out.println(this.status);
		status.put(LocalDateTime.now(), VehicleLocationHistoryDTO.builder()
				.location(new SimpleObjectProperty<>(this.status.values().isEmpty() ? null : this.status.values().iterator().next().getLocation()))
				.status(new SimpleObjectProperty<>(vehicleStatus))
				.build()
		);
	}


	@Override
	public String toString() {
		return plate.get();
	}

}
