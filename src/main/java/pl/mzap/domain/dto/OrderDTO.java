package pl.mzap.domain.dto;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.*;
import pl.mzap.domain.status.OrderStatus;

import java.util.stream.Collectors;

@Builder
public class OrderDTO {

	private ObjectProperty<ContractorDTO> contractor;
	private StringProperty relation;
	private StringProperty referenceNumber;
	private DoubleProperty paymentAmount;
	private IntegerProperty paymentTermInDays;
	private ListProperty<CargoDTO> cargos;
	private ListProperty<TransportDTO> transports;
	private ObjectProperty<OrderStatus> status;

	public ContractorDTO getContractor() {
		return contractor.get();
	}
	public String getRelation() {
		return relation.get();
	}
	public String getReferenceNumber() {
		return referenceNumber.get();
	}
	public double getPaymentAmount() {
		return paymentAmount.get();
	}
	public int getPaymentTermInDays() {
		return paymentTermInDays.get();
	}
	public ObservableList<CargoDTO> getCargos() {
		return cargos.get();
	}
	public ObservableList<TransportDTO> getTransports() {
		return transports.get();
	}
	public OrderStatus getStatus() {
		return status.get();
	}

	public ObjectProperty<ContractorDTO> contractorProperty() {
		return contractor;
	}
	public StringProperty relationProperty() {
		return relation;
	}
	public StringProperty referenceNumberProperty() {
		return referenceNumber;
	}
	public DoubleProperty paymentAmountProperty() {
		return paymentAmount;
	}
	public IntegerProperty paymentTermInDaysProperty() {
		return paymentTermInDays;
	}

	public void setContractor(ContractorDTO contractor) {
		this.contractor = new SimpleObjectProperty<>(contractor);
	}
	public void setRelation(String relation) {
		this.relation = new SimpleStringProperty(relation);
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = new SimpleStringProperty(referenceNumber);
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = new SimpleDoubleProperty(paymentAmount);
	}
	public void setPaymentTermInDays(int paymentTermInDays) {
		this.paymentTermInDays = new SimpleIntegerProperty(paymentTermInDays);
	}
	public void setCargos(ObservableList<CargoDTO> cargos) {
		this.cargos = new SimpleListProperty<>(cargos);
	}
	public void setTransports(ObservableList<TransportDTO> transports) {
		this.transports = new SimpleListProperty<>(transports);
	}
	public void setStatus(OrderStatus status) {
		this.status = new SimpleObjectProperty<>(status);
	}

}
