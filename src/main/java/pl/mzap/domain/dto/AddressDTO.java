package pl.mzap.domain.dto;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Builder;

@Builder
public class AddressDTO {

    private StringProperty city;
    private StringProperty street;
    private StringProperty building;
    private StringProperty postalCode;

    public String getCity() {
        return city.get();
    }
    public StringProperty cityProperty() {
        return city;
    }
    public String getStreet() {
        return street.get();
    }
    public StringProperty streetProperty() {
        return street;
    }

    public String getBuilding() {
        return building.get();
    }
    public StringProperty buildingProperty() {
        return building;
    }
    public String getPostalCode() {
        return postalCode.get();
    }
    public StringProperty postalCodeProperty() {
        return postalCode;
    }

    public void setCity(String city) {
        this.city = new SimpleStringProperty(city);
    }
    public void setStreet(String street) {
        this.street = new SimpleStringProperty(street);
    }
    public void setBuilding(String building) {
        this.building = new SimpleStringProperty(building);
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = new SimpleStringProperty(postalCode);
    }

}