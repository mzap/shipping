package pl.mzap.domain.dto;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Builder;
import pl.mzap.domain.kind.TransportAction;

import java.time.LocalDateTime;

@Builder
public class TransportDTO {

	private ObjectProperty<TransportAction> transportAction;
	private StringProperty referenceNumber;
	private ObjectProperty<LocalDateTime> date;
	private StringProperty companyName;
	private ObjectProperty<CargoDTO> cargo;
	private ObjectProperty<VehicleDTO> vehicle;
	private ObjectProperty<AddressDTO> address;

	public TransportAction getTransportAction() {
		return transportAction.get();
	}
	public String getReferenceNumber() {
		return referenceNumber.get();
	}
	public String getCompanyName() {
		return companyName.get();
	}
	public CargoDTO getCargo() {
        return cargo.get();
    }
    public VehicleDTO getVehicle() {
        return vehicle.get();
    }
    public LocalDateTime getDate() {
		return date.get();
	}
	public AddressDTO getAddress() {
		return address.get();
	}

	public void setTransportAction(TransportAction transportAction) {
		this.transportAction = new SimpleObjectProperty<>(transportAction);
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = new SimpleStringProperty(referenceNumber);
	}
	public void setCompanyName(String companyName) {
		this.companyName = new SimpleStringProperty(companyName);
	}
	public void setCargo(CargoDTO cargo) {
        this.cargo = new SimpleObjectProperty<>(cargo);
    }
    public void setVehicle(VehicleDTO vehicle) {
        this.vehicle = new SimpleObjectProperty<>(vehicle);
    }
    public void setDate(LocalDateTime date) {
		this.date = new SimpleObjectProperty<>(date);
	}
	public void setAddress(AddressDTO address) {
		this.address = new SimpleObjectProperty<>(address);
	}

}
