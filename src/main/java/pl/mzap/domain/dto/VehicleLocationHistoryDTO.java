package pl.mzap.domain.dto;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Builder;
import pl.mzap.domain.status.VehicleStatus;

@Builder
public class VehicleLocationHistoryDTO {

    private ObjectProperty<AddressDTO> location;
    private ObjectProperty<VehicleStatus> status;

    public AddressDTO getLocation() {
        return location.get();
    }
    public VehicleStatus getStatus() {
        return status.get();
    }

    public ObjectProperty<AddressDTO> locationProperty() {
        return location;
    }
    public ObjectProperty<VehicleStatus> statusProperty() {
        return status;
    }

    public void setLocation(AddressDTO location) {
        this.location = new SimpleObjectProperty<>(location);
    }
    public void setStatus(VehicleStatus status) {
        this.status = new SimpleObjectProperty<>(status);
    }
}
