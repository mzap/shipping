package pl.mzap.domain.dto;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Builder;

@Builder
public class CargoDTO {

	private StringProperty name;
	private StringProperty description;
	private StringProperty number;

	public String getName() {
		return name.get();
	}
	public String getDescription() {
		return description.get();
	}
	public String getNumber() {
		return number.get();
	}

	public void setName(String name) {
		this.name = new SimpleStringProperty(name);
	}
	public void setDescription(String description) {
		this.description = new SimpleStringProperty(description);
	}
	public void setNumber(String number) {
		this.number = new SimpleStringProperty(number);
	}

	@Override
	public String toString() {
		return name.get();
	}
}
