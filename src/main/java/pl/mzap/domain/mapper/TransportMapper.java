package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import pl.mzap.domain.dto.TransportDTO;
import pl.mzap.domain.model.Transport;
import pl.mzap.domain.model.Vehicle;

public class TransportMapper {

	public static TransportDTO getTransportDTO(Transport transport) {
		return TransportDTO.builder()
				.address(new SimpleObjectProperty<>(AddressMapper.getAddressDTO(transport.getAddress())))
				.referenceNumber(new SimpleStringProperty(transport.getReferenceNumber()))
				.transportAction(new SimpleObjectProperty<>(transport.getTransportAction()))
				.date(new SimpleObjectProperty<>(transport.getDate()))
				.companyName(new SimpleStringProperty(transport.getCompanyName()))
				.cargo(new SimpleObjectProperty<>(CargoMapper.getCargoDTO(transport.getCargo())))
				.vehicle(new SimpleObjectProperty<>(VehicleMapper.getVehicleDTO(transport.getVehicle())))
				.build();
	}

}
