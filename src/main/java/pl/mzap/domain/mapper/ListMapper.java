package pl.mzap.domain.mapper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.mzap.domain.dto.CargoDTO;
import pl.mzap.domain.dto.TransportDTO;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.model.Cargo;
import pl.mzap.domain.model.Transport;
import pl.mzap.domain.model.Vehicle;

import java.util.List;
import java.util.stream.Collectors;

public class ListMapper {

	public static ObservableList<CargoDTO> getCargos(List<Cargo> cargos) {
		return cargos.stream()
				.map(CargoMapper::getCargoDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}

	public static ObservableList<VehicleDTO> getVehicles(List<Vehicle> vehicles) {
		return vehicles.stream()
				.map(VehicleMapper::getVehicleDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}

	public static ObservableList<TransportDTO> getTransports(List<Transport> transports) {
		return transports.stream()
				.map(TransportMapper::getTransportDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}
}
