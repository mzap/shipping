package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleObjectProperty;
import pl.mzap.domain.dto.VehicleLocationHistoryDTO;
import pl.mzap.domain.model.VehicleLocationHistory;

public class VehicleLocationMapper {

    public static VehicleLocationHistoryDTO getVehicleLocationDTO(VehicleLocationHistory vehicleLocationHistory) {
        return VehicleLocationHistoryDTO.builder()
                .location(new SimpleObjectProperty<>(AddressMapper.getAddressDTO(vehicleLocationHistory.getLocation())))
                .status(new SimpleObjectProperty<>(vehicleLocationHistory.getStatus()))
                .build();
    }
}
