package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.model.Vehicle;

public class VehicleMapper {

	public static VehicleDTO getVehicleDTO(Vehicle vehicle) {
		return VehicleDTO.builder()
				.plate(new SimpleStringProperty(vehicle.getPlate()))
				.driver(new SimpleStringProperty(vehicle.getDriver()))
				.vehicleKind(new SimpleObjectProperty<>(vehicle.getVehicleKind()))
				.status(new SimpleMapProperty<>(MapMapper.getStatus(vehicle.getStatus())))
				.build();
	}

}
