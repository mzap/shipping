package pl.mzap.domain.mapper;

import javafx.beans.property.*;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.model.Order;

public class OrderMapper {

	public static OrderDTO getOrderDTO(Order order) {
		return OrderDTO.builder()
				.contractor(new SimpleObjectProperty<>(ContractorMapper.getContractorDTO(order.getContractor())))
				.relation(new SimpleStringProperty(order.getRelation().getName()))
				.referenceNumber(new SimpleStringProperty(order.getReferenceNumber()))
				.paymentAmount(new SimpleDoubleProperty(order.getPaymentAmount()))
				.paymentTermInDays(new SimpleIntegerProperty(order.getPaymentTermInDays()))
				.cargos(new SimpleListProperty<>(ListMapper.getCargos(order.getCargos())))
				.transports(new SimpleListProperty<>(ListMapper.getTransports(order.getTransports())))
				.status(new SimpleObjectProperty<>(order.getStatus().values().iterator().next()))
				.build();
	}
}
