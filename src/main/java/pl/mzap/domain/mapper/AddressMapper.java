package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleStringProperty;
import pl.mzap.domain.dto.AddressDTO;
import pl.mzap.domain.model.Address;

public class AddressMapper {

	public static AddressDTO getAddressDTO(Address address) {
		return AddressDTO.builder()
				.city(new SimpleStringProperty(address.getCity()))
				.street(new SimpleStringProperty(address.getStreet()))
				.building(new SimpleStringProperty(address.getBuilding()))
				.postalCode(new SimpleStringProperty(address.getPostalCode()))
				.build();
	}

}
