package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleStringProperty;
import pl.mzap.domain.dto.CargoDTO;
import pl.mzap.domain.model.Cargo;

public class CargoMapper {

	public static CargoDTO getCargoDTO(Cargo cargo) {
		return CargoDTO.builder()
				.name(new SimpleStringProperty(cargo.getName()))
				.description(new SimpleStringProperty(cargo.getDescription()))
				.number(new SimpleStringProperty(cargo.getNumber()))
				.build();
	}

}
