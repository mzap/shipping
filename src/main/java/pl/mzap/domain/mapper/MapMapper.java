package pl.mzap.domain.mapper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import pl.mzap.domain.dto.VehicleLocationHistoryDTO;
import pl.mzap.domain.model.VehicleLocationHistory;

import java.time.LocalDateTime;
import java.util.Map;

public class MapMapper {

    public static ObservableMap<LocalDateTime, VehicleLocationHistoryDTO> getStatus(Map<LocalDateTime, VehicleLocationHistory> status) {
        ObservableMap<LocalDateTime, VehicleLocationHistoryDTO> statusHistory = FXCollections.observableHashMap();
        status
                .forEach((key, value) -> statusHistory.put(key, VehicleLocationMapper.getVehicleLocationDTO(value)));
        return statusHistory;
    }
}
