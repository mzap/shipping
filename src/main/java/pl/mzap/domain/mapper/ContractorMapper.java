package pl.mzap.domain.mapper;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.model.Contractor;

public class ContractorMapper {

	public static ContractorDTO getContractorDTO(Contractor contractor) {
		return ContractorDTO.builder()
				.name(new SimpleStringProperty(contractor.getName()))
				.shortName(new SimpleStringProperty(contractor.getShortName()))
				.phoneNumber(new SimpleStringProperty(contractor.getPhoneNumber()))
				.vatId(new SimpleStringProperty(contractor.getVatId()))
				.address(new SimpleObjectProperty<>(AddressMapper.getAddressDTO(contractor.getAddress())))
				.shortName(new SimpleStringProperty(contractor.getShortName()))
				.status(new SimpleObjectProperty<>(contractor.getStatus().values().iterator().next()))
				.build();
	}

}
