package pl.mzap.domain.status;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum VehicleStatus {

    ACTIVE("Active", "A"),
    LOADING("Loading", "L"),
    UNLOADING("Unloading", "U"),
    REALIZE("Realize", "R");

    private String name;
    private String status;

    public static ObservableList<VehicleStatus> getStatuses() {
        return FXCollections.observableArrayList(Arrays.asList(values()));
    }

    @Override
    public String toString() {
        return name;
    }

}
