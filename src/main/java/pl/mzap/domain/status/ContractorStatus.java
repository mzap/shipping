package pl.mzap.domain.status;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum ContractorStatus {

	REGISTERED("Registered", "R"),
	VERIFIED("Verified", "R"),
	ACCEPTED("Accepted", "A"),
	DELETED("Deleted", "D");

	private String name;
	private String status;

	public static ObservableList<ContractorStatus> getStatuses() {
		return FXCollections.observableArrayList(Arrays.asList(values()));
	}

	@Override
	public String toString() {
		return name;
	}
}
