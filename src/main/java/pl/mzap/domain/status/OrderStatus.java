package pl.mzap.domain.status;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum OrderStatus {

    NEW("New", "N"),
    ACCEPTED("Accepted", "A"),
    REALIZE("Realized", "R"),
    CANCELED("Canceled", "C"),
    DONE("Done", "D");

    private String name;
    private String status;

	public static ObservableList<OrderStatus> getStatuses() {
		return FXCollections.observableArrayList(Arrays.asList(values()));
	}

	@Override
	public String toString() {
		return name;
	}
}