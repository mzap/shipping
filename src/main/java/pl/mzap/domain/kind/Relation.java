package pl.mzap.domain.kind;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Relation {

	TO("To", "T"),
	FROM("From", "F");

	private String name;
	private String code;

	public static ObservableList<Relation> getRelations() {
		return FXCollections.observableArrayList(Arrays.asList(values()));
	}

	@Override
	public String toString() {
		return name;
	}
}
