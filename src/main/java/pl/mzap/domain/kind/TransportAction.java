package pl.mzap.domain.kind;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TransportAction {

	LOADING("Loading", "L"),
	UNLOADING("Unloading", "U");

	private String name;
	private String code;

	public static ObservableList<TransportAction> getTransportActions() {
		return FXCollections.observableArrayList(Arrays.asList(values()));
	}

	@Override
	public String toString() {
		return name;
	}
}
