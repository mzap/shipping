package pl.mzap.domain.kind;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum VehicleKind {

	PATELNIA("Patelnia", "P"),
	PLANDEKA("Plandeka", "L"),
	PODWOJNA_PLANDEKA("Podwojna plandeka", "PL");

	private String name;
	private String code;

	public static ObservableList<VehicleKind> getVehicleKinds() {
		return FXCollections.observableArrayList(Arrays.asList(values()));
	}

	@Override
	public String toString() {
		return name;
	}

}
