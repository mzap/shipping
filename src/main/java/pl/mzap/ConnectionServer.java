package pl.mzap;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Tworzy nowy watek ktory czeka na polaczenie z klientem.
 * Po uzyskaniu polaczenia przekazuje sterowanie do nowego watku ktory obsluguje polaczenie z polaczym klientem i z baza danych
 * @see EchoThread
 */
@Deprecated
public class ConnectionServer extends Thread {

	private ServerSocket serverSocket;
	private Socket clientSocket;
	
	@Override
	public void run(){
		try {
			serverSocket = new ServerSocket(6789);
		} catch (IOException e) {
			System.err.println("Blad przy tworzeniu serverSocket");
		}
		while(true){
			try {
				clientSocket = serverSocket.accept();
				System.err.println("Polaczono z klientem");
			} catch (IOException e) {
				System.err.println("Blad przy akceptowaniu polaczenia");
			}
			new EchoThread(clientSocket).start();
		}
		
	}
	
	public void closeConnection(){
		try {
			serverSocket.close();
			clientSocket.close();
		} catch (IOException e) {
			System.err.println("Blad przy zamykaniu polaczenia");
		}
	}

}
