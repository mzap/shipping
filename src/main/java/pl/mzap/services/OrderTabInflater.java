package pl.mzap.services;

import pl.mzap.controller.*;
import pl.mzap.domain.dto.OrderDTO;

public class OrderTabInflater {

	protected void inflateOrderSummaryGrid(OrderSummaryController summaryController, OrderDTO order) {
		summaryController.setOrder(order);
		summaryController.inflateTemplate();
		summaryController.initTemplateListener();
	}

	protected void inflateOrderVehiclesTable(OrderVehiclesController vehiclesController, OrderDTO order) {
		vehiclesController.inflateTable(order);
	}

	protected void inflateOrderCargosTable(OrderCargosController cargosController, OrderDTO order) {
		cargosController.inflateTable(order);
	}

	protected void inflateOrderTransportsTable(OrderTransportsController transportsController, OrderDTO order) {
		transportsController.inflateTable(order);
	}

	protected void inflateOrderHistoryList(OrderHistoryController historyController, OrderDTO order) {
		historyController.inflateList(order);
	}

}
