package pl.mzap.util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.VehicleDTO;

import java.util.Collection;

@Log4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Context {

	public static Context INSTANCE = new Context();

	private ObservableList<OrderDTO> orders = FXCollections.observableArrayList();
	private ObservableList<ContractorDTO> contractors = FXCollections.observableArrayList();
	private ObservableList<VehicleDTO> vehicles = FXCollections.observableArrayList();

	public ObservableList<OrderDTO> getOrders() {
		log.debug("[GET] - orders");
		return orders;
	}
	public ObservableList<VehicleDTO> getVehicles() {
		log.debug("[GET] - vehicles");
		return vehicles;
	}
	public ObservableList<ContractorDTO> getContractors() {
		log.debug("[GET] - contractors");
		return contractors;
	}

	public void setOrders(Collection<OrderDTO> orders){
		log.debug("[SET] - orders");
		this.orders.setAll(orders);
	}
	public void setContractors(Collection<ContractorDTO> contractors){
		log.debug("[SET] - contractors");
		this.contractors.setAll(contractors);
	}
	public void setVehicles(Collection<VehicleDTO> vehicles){
		log.debug("[SET] - vehicles");
		this.vehicles.setAll(vehicles);
	}

	public void addOrder(OrderDTO order) {
		log.debug("[ADD] - orders");
		this.orders.add(order);
	}
	public void addContractors(ContractorDTO contractor){
		log.debug("[ADD] - contractors");
		this.contractors.add(contractor);
	}
	public void addVehicle(VehicleDTO vehicle) {
		log.debug("[ADD] - vehicle");
		this.vehicles.add(vehicle);
	}

	public void removeOrder(OrderDTO order) {
		log.debug("[DEL] - orders");
		this.orders.remove(order);
	}
	public void removeContractor(ContractorDTO contractor ) {
		log.debug("[DEL] - contractor");
		this.contractors.remove(contractor);
	}
	public void removeVehicle(VehicleDTO vehicle) {
		log.debug("[DEL] - vehicle");
		this.vehicles.remove(vehicle);
	}

}
