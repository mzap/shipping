package pl.mzap.util;

public class Constant {

	public static final String APP_NAME = "Shipping";
	public static final String VEHICLES_WINDOW_NAME = "Fleet";
	public static final String ORDER_WINDOW_NAME = "Order";
	public static final String ADD_CARGO_WINDOW_NAME = "Add cargo";
	public static final String ADD_TRANSPORT_WINDOW_NAME = "Add transport";
	public static final String ADD_VEHICLE_WINDOW_NAME = "Add vehicle";
	public static final String ADD_CONTRACTOR_WINDOW_NAME = "Add contractor";
	public static final String CONTRACTOR_WINDOW_NAME = "Contractors";

	public static final String SPACE = " ";
	public static final String ARROW = " -> ";

	public static final String GOOGLE_MAPS_API_KEY = "AIzaSyCpxuW2Vp-njtDeQeEU_NW9yV_In-Qossc";


}
