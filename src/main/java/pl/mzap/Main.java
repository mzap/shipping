package pl.mzap;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.extern.log4j.Log4j;
import pl.mzap.controller.*;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.generator.DTOGenerator;
import pl.mzap.util.Constant;
import pl.mzap.util.Context;

import java.io.IOException;

@Log4j
public class Main extends Application {

	private Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		generateData();
		try {
			showOrdersView();
		} catch (IOException e) {
			log.warn("ERROR in MAIN " + e.getMessage());
		}
	}

	private void generateData() {
		Context.INSTANCE.setOrders(DTOGenerator.generateOrders(10, 2, 3));
		Context.INSTANCE.setContractors(DTOGenerator.generateContractors(10));
		Context.INSTANCE.setVehicles(DTOGenerator.generateVehicles(10));
	}

	private void showOrdersView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Orders.fxml"));
		AnchorPane rootLayout = loader.load();

		OrdersController controller = loader.getController();
		controller.setMain(this);

		Scene scene = new Scene(rootLayout);

		primaryStage.setScene(scene);
		primaryStage.setTitle(Constant.APP_NAME);
		primaryStage.show();
	}

	public void showOrderView(OrderDTO selectedOrder) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/order/Order.fxml"));
		BorderPane order = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.ORDER_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.setScene(new Scene(order));

		OrderController controller = loader.getController();
		controller.setMain(this);

		inflateOrderTabPanes(controller);
		controller.setOrder(selectedOrder);
		controller.inflateOrderStatusComboBox();
		controller.inflateTabPanesTemplate();

		dialogStage.show();
	}

	private void inflateOrderTabPanes(OrderController controller) throws IOException {
		controller.setSummaryController((OrderSummaryController) inflateTabPaneAndGetController(controller.getSummaryTabPane(), "OrderSummary"));
		controller.setVehiclesController((OrderVehiclesController) inflateTabPaneAndGetController(controller.getVehiclesTabPane(), "OrderVehicles"));
		controller.setCargosController((OrderCargosController) inflateTabPaneAndGetController(controller.getCargosTabPane(), "OrderCargos"));
		controller.setTransportsController((OrderTransportsController) inflateTabPaneAndGetController(controller.getTransportsTabPane(), "OrderTransports"));
		controller.setHistoryController((OrderHistoryController) inflateTabPaneAndGetController(controller.getHistoryTabPane(), "OrderHistory"));
	}

	private Object inflateTabPaneAndGetController(AnchorPane tabPane, String viewFileName) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/order/" + viewFileName + ".fxml"));
		Object loadedPane = loader.load();
		tabPane.getChildren().add((Node) loadedPane);
		return loader.getController();
	}

	public void showAddCargoView(OrderDTO selectedOrder) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/order/AddCargo.fxml"));
		AnchorPane cargo = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.ADD_CARGO_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.setScene(new Scene(cargo));

		AddCargoController controller = loader.getController();

		controller.setDialogStage(dialogStage);
		controller.setOrder(selectedOrder);
		dialogStage.show();
	}

	public void showAddTransportView(OrderDTO selectedOrder) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/order/AddTransport.fxml"));
		AnchorPane transport = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.ADD_TRANSPORT_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.setScene(new Scene(transport));

		AddTransportController controller = loader.getController();

		controller.setDialogStage(dialogStage);
		controller.setOrder(selectedOrder);
		controller.inflateCargoComboBox();
		dialogStage.show();
	}

	public void showVehiclesWindow() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/view/vehicle/Vehicles.fxml"));
		BorderPane vehicles = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.VEHICLES_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);

		Scene scene = new Scene(vehicles);
		dialogStage.setScene(scene);

		VehicleController controller = loader.getController();
		controller.setMain(this);

		dialogStage.show();
	}

	public void showAddVehicleView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/vehicle/AddVehicle.fxml"));
		AnchorPane transport = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.ADD_VEHICLE_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.setScene(new Scene(transport));

		AddVehicleController controller = loader.getController();

		controller.setDialogStage(dialogStage);
		dialogStage.show();
	}

	public void showContractorsWindow() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/view/contractor/Contractors.fxml"));
		BorderPane customers = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.CONTRACTOR_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);

		Scene scene = new Scene(customers);
		dialogStage.setScene(scene);

		ContractorController controller = loader.getController();
		controller.setMain(this);

		dialogStage.show();
	}

	public void showAddContractorWindow() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/contractor/AddContractor.fxml"));
		AnchorPane transport = loader.load();

		Stage dialogStage = new Stage();
		dialogStage.setTitle(Constant.ADD_CONTRACTOR_WINDOW_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.setScene(new Scene(transport));

		AddContractorController controller = loader.getController();

		controller.setDialogStage(dialogStage);
		dialogStage.show();
	}

}