package pl.mzap.database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class DatabaseAccess {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public void openConnection() {
        entityManagerFactory = Persistence.createEntityManagerFactory("hibernate_h2_conf");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public void closeConnection() {
        entityManager.close();
        entityManagerFactory.close();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

}
