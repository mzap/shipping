package pl.mzap.generator;

import pl.mzap.domain.kind.Relation;
import pl.mzap.domain.kind.TransportAction;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.model.*;
import pl.mzap.domain.status.ContractorStatus;
import pl.mzap.domain.status.OrderStatus;
import pl.mzap.domain.status.VehicleStatus;
import pl.mzap.generator.data.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class DataGenerator {

	private static Random rand = new Random();
	private static Integer addressDataLength = AddressData.values().length;
	private static Integer cargoDataLength = CargoData.values().length;
	private static Integer contractorDataLength = ContractorData.values().length;
	private static Integer orderReferenceNumberDataLength = OrderReferenceNumberData.values().length;
	private static Integer transportDataLength = TransportData.values().length;
	private static Integer transportActionLength = TransportAction.values().length;
	private static Integer vehicleDataLength = VehicleData.values().length;
	private static Integer vehicleKindLength = VehicleKind.values().length;
	private static Integer relationLength = Relation.values().length;
	private static Integer contractorStatusLength = ContractorStatus.values().length;
	private static Integer orderStatusLength = OrderStatus.values().length;
	private static Integer vehicleStatusLength = VehicleStatus.values().length;

	public static List<Contractor> generateContractors(int numberOfContractors) {
		List<Contractor> contractors = new ArrayList<>();
		IntStream.range(0, numberOfContractors)
				.forEach(value -> contractors.add(generateContractor()));
		return contractors;
	}

	public static List<Order> generateOrders(int numberOfOrders, int maxNumberOfTransports, int maxNumberOfCargos) {
		List<Order> orders = new ArrayList<>();
		IntStream.range(0, numberOfOrders)
				.forEach(i -> orders.add(generateOrder(rand.nextInt(maxNumberOfTransports) + 1, rand.nextInt(maxNumberOfCargos) + 1)));
		return orders;
	}

	public static List<Vehicle> generateVehicles(int maxNumberOfCars) {
		List<Vehicle> vehicles = new ArrayList<>();
		IntStream.range(0, maxNumberOfCars)
				.forEach(value -> vehicles.add(generateVehicle()));
		return vehicles;
	}

	private static Order generateOrder(int numberOfTransports, int maxNumberOfCargos) {
		List<Cargo> cargos = generateCargos(maxNumberOfCargos);
		return Order.builder()
				.contractor(generateContractor())
				.relation(generateRelation())
				.referenceNumber(generateReferenceNumber())
				.paymentAmount(generatePaymentAmount())
				.paymentTermInDays(generatePaymentTermInDays())
				.cargos(cargos)
				.transports(generateTransports(numberOfTransports, cargos))
				.status(generateOrderStatus())
				.build();
	}

	private static List<Cargo> generateCargos(int numberOfCargos) {
		List<Cargo> cargos = new ArrayList<>();
		IntStream.range(0, numberOfCargos)
				.forEach(value -> cargos.add(generateCargo()));
		return cargos;
	}

	private static List<Transport> generateTransports(int numberOfTransports, List<Cargo> generatedCargo) {
		List<Transport> transports = new ArrayList<>();
		IntStream.range(0, numberOfTransports)
				.forEach(value -> transports.add(generateTransport(generatedCargo)));
		return transports;
	}

	private static LinkedHashMap<LocalDateTime, ContractorStatus> generateContractorStatus() {
		ContractorStatus contractorStatus = ContractorStatus.values()[rand.nextInt(contractorStatusLength - 1)];
		return new LinkedHashMap<LocalDateTime, ContractorStatus>() {{
			put(LocalDateTime.now(), contractorStatus);
		}};
	}

	private static LinkedHashMap<LocalDateTime, OrderStatus> generateOrderStatus() {
		OrderStatus ordersStatus = OrderStatus.values()[rand.nextInt(orderStatusLength - 1)];
		return new LinkedHashMap<LocalDateTime, OrderStatus>() {{
			put(LocalDateTime.now(), ordersStatus);
		}};
	}

	private static LinkedHashMap<LocalDateTime, VehicleLocationHistory> generateVehicleStatus() {
		return new LinkedHashMap<LocalDateTime, VehicleLocationHistory>() {{
			put(LocalDateTime.now(), generateVehicleLocationHistory());
		}};
	}

	private static VehicleLocationHistory generateVehicleLocationHistory() {
		VehicleStatus vehicleStatus = VehicleStatus.values()[rand.nextInt(vehicleStatusLength - 1)];
		return VehicleLocationHistory
				.builder()
				.location(generateAddress())
				.status(vehicleStatus)
				.build();
	}

	private static VehicleKind generateVehicleKind() {
		return VehicleKind.values()[rand.nextInt(vehicleKindLength - 1)];
	}

	private static Cargo generateCargo() {
		CargoData cargo = CargoData.values()[rand.nextInt(cargoDataLength - 1)];
		return Cargo.builder()
				.name(cargo.getName())
				.description(cargo.getDescription())
				.number(cargo.getNumber())
				.build();
	}

	private static Vehicle generateVehicle() {
		int random = rand.nextInt(vehicleDataLength - 1);
		return Vehicle.builder()
				.plate(VehicleData.values()[random].getPlate())
				.driver(VehicleData.values()[random].getDriver())
				.vehicleKind(generateVehicleKind())
				.status(generateVehicleStatus())
				.build();
	}

	private static Transport generateTransport(List<Cargo> generatedCargo) {
		TransportData transportData = TransportData.values()[rand.nextInt(transportDataLength - 1)];
		return Transport.builder()
				.referenceNumber(transportData.getReferenceNumber())
				.companyName(generateContractor().getName())
				.address(generateAddress())
				.transportAction(generateTransportAction())
				.date(LocalDateTime.now().plusDays(rand.nextInt(5) + 1))
				.cargo(generatedCargo.get(rand.nextInt(generatedCargo.size())))
				.vehicle(generateVehicle())
				.build();
	}

	private static TransportAction generateTransportAction() {
		return TransportAction.getTransportActions().get(rand.nextInt(transportActionLength - 1));
	}

	private static Contractor generateContractor() {
		ContractorData contractorData = ContractorData.values()[rand.nextInt(contractorDataLength - 1)];
		return Contractor.builder()
				.name(contractorData.getName())
				.shortName(contractorData.getShortName())
				.vatId(contractorData.getVatId())
				.phoneNumber(contractorData.getPhoneNumber())
				.address(generateAddress())
				.status(generateContractorStatus())
				.build();
	}

	private static Relation generateRelation() {
		return Relation.values()[rand.nextInt(relationLength - 1)];
	}

	private static String generateReferenceNumber() {
		return OrderReferenceNumberData.values()[rand.nextInt(orderReferenceNumberDataLength - 1)].getNumber();
	}

	private static Double generatePaymentAmount() {
		return Math.ceil(rand.nextInt(500000) + 10000 + rand.nextDouble() + 1);
	}

	private static Integer generatePaymentTermInDays() {
		return rand.nextInt(80) + 1;
	}

	private static Address generateAddress() {
		AddressData addressData = AddressData.values()[rand.nextInt(addressDataLength - 1)];
		return Address.builder()
				.city(addressData.getCity())
				.street(addressData.getStreet())
				.building(addressData.getBuilding())
				.postalCode(addressData.getPostalCode())
				.build();
	}

}