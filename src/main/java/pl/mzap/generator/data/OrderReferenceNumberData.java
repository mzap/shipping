package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderReferenceNumberData {

	NO1("123-CSCSI-1-CS"),
	NO2("954-VDAAA-1-KK"),
	NO3("522-NOOSK-1-AS"),
	NO4("643-PBISA-1-VD");

	private String number;

}
