package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.model.Vehicle;

@Getter
@AllArgsConstructor
public enum VehicleData {

	TK1("TK-45867", "Michalski"),
	WW1("WW-12932", "Pisarek"),
	KR1("KR-83742", "Norkowski"),
	KR2("KR-11228", "Pasek"),
	KR3("KR-18666", "Bonirowski"),
	WW2("WW-34644", "Paczebski"),
	WW3("WW-89932", "Gowionski"),
	SK1("SK-02392", "Franczek");

	private String plate;
	private String driver;

}
