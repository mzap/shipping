package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.mzap.domain.model.Contractor;

@Getter
@AllArgsConstructor
public enum ContractorData {

	Kowalski("Kowal&Bowal", "KB", "12-321-1323", "503-103-104"),
	Michalski("MetroTrans", "MT", "123-123-123", "293-968-392"),
	Nowakowski("Nowaczek.CO", "NW", "123-45-887", "600-040-123"),
	Podoslki("Padolek", "PP", "74-2-234-55", "500-555-123");

	private String name;
	private String shortName;
	private String vatId;
	private String phoneNumber;

}
