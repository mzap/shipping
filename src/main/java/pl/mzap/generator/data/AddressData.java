package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import pl.mzap.domain.model.Address;

@Getter
@AllArgsConstructor
public enum AddressData {

	Warszawa("Warszawa", "Jezioranskiego", "16", "00-560"),
	Krakow("Krakow", "Wawelska", "17", "44-612"),
	Kielce("Kielce", "Sandomierska", "77", "11-543"),
	Wroclaw("Wroclaw", "Mickiewicza", "16/2", "63-705"),
	Gdansk("Gdansk", "Sopocka", "5", "12-600");

	private String city;
	private String street;
	private String building;
	private String postalCode;

}
