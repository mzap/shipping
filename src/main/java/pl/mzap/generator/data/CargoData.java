package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CargoData {

	Elektronika("Elektronika", "Palety elektroniki, pakowane w kartonach 120x120. Ostroznie przy zaladunku", "1YX-177"),
	Porcelana("Porcelana", "Uwaga przy zaladunku, produkt wysokiego ryzyka", "7GD-821"),
	Meble("Meble", "Produkt w czesciach. Zabezpieczony plastikowym oslonami", "6RE-921"),
	Dywany("Dywany", "Zrolowane po 12szt. na przedluzonych paletach", "00R-103"),
	Owoce("Owoce", "Transportowac w temperaturze 6 stopni", "4RQ-815");

	private String name;
	private String description;
	private String number;

}
