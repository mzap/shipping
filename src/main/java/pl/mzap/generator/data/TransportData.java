package pl.mzap.generator.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransportData {

	NO1("12/18/2018/1"),
	NO2("11/25/2018/4"),
	NO3("09/05/2018/8");

	private String referenceNumber;

}
