package pl.mzap.generator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.mapper.ContractorMapper;
import pl.mzap.domain.mapper.OrderMapper;
import pl.mzap.domain.mapper.VehicleMapper;

import java.util.stream.Collectors;

public class DTOGenerator {

	public static ObservableList<OrderDTO> generateOrders(int numberOfOrders, int maxNumberOfTransports, int maxNumberOfCargos) {
		return DataGenerator.generateOrders(numberOfOrders, maxNumberOfTransports, maxNumberOfCargos).parallelStream()
				.map(OrderMapper::getOrderDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}

	public static ObservableList<ContractorDTO> generateContractors(int numberOfContractors) {
		return DataGenerator.generateContractors(numberOfContractors).parallelStream()
				.map(ContractorMapper::getContractorDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}

	public static ObservableList<VehicleDTO> generateVehicles(int numberOfVehicles) {
		return DataGenerator.generateVehicles(numberOfVehicles).parallelStream()
				.map(VehicleMapper::getVehicleDTO)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}

}
