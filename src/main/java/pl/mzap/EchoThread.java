package pl.mzap;
//
//import pl.mzap.model.Cargos;
//import pl.mzap.model.Customers;
//import pl.mzap.model.Orders;
//import pl.mzap.model.OrdersClient;
//
//import java.io.*;
import java.net.Socket;
//import java.util.ArrayList;
//import java.util.Date;
//
///**
// * Watek obslugujacy polaczenie z klientem oraz baza danych.
// * Odbiera wszystkie dane wyslane przez klienta oraz pobiera odpowiednie dane z bazy danych lub je tam zapisuje.
// * @see ConnectionServer
// */
@Deprecated
public class EchoThread extends Thread{
//
	protected Socket clientSocket;
//	private ObjectInputStream receiveObject;
//	private ObjectOutputStream sendObject;
//	private BufferedReader receive;
//	private PrintWriter send;
//
//	private static Database db = new Database();
//
//	private Customers customer;
//	private Customers sender;
//	private Customers recipient;
//	private Cargos cargo;
//	private boolean connected = true;
//	private int flag;
//
	public EchoThread(){}

	public EchoThread(Socket clientSocket){
		this.clientSocket = clientSocket;
	}

//	@Override
//	public void run(){
//		db.connect();
//		try {
//			receiveObject = new ObjectInputStream(clientSocket.getInputStream());
//			sendObject = new ObjectOutputStream(clientSocket.getOutputStream());
//			receive = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//			send = new PrintWriter(clientSocket.getOutputStream());
//		} catch (IOException e) {
//			System.err.println("Blad przy tworzeniu strumienia");
//		}
//		while(connected){
//
//			flag = -1;
//
//			try {
//				flag = Integer.parseInt(receive.readLine());
//			} catch (IOException e) {
//				System.err.println("Blad przy odbieraniu flagi od klienta");
//				closeConnection();
//			}
//
//			System.out.println("FLAGA: "+flag);
//
//			switch(flag){
//				case 1:
//					login();
//					break;
//				case 2:
//					register();
//					break;
//				case 3:
//					newOrder();
//					break;
//				case 4:
//					mineOrders();
//					break;
//				case 5:
//					updateCustomer();
//					break;
//				default:
//					System.err.println("Zamykam polaczenie. Flaga nieskonfigurowana");
//					closeConnection();
//				break;
//
//			}
//
//		}
//
//	}
//
//	public void closeConnection(){
//		try {
//			connected = false;
//			clientSocket.close();
//			receiveObject.close();
//			receive.close();
//			send.close();
//		} catch (IOException e) {
//			System.err.println("Blad przy zamykaniu polaczenia przez EchoThread server");
//		}
//	}
//
//
//	private void updateCustomer() {
//
//		customer = null;
//		try {
//			customer = (Customers) receiveObject.readObject();
//		} catch (ClassNotFoundException | IOException e) {
//			System.err.println("Blad podczas osbierania obiektu do aktualizacji");
//		}
//
//		if(customer != null){
//			System.out.println(customer + customer.getPassword());
//
//			if(customer.getPassword().equals("")){
//				db.updateCustomer(customer);
//			}else{
//				db.updatePasswordCustomer(customer);
//			}
//
//			send.println("1");
//			send.flush();
//		}
//
//
//
//	}
//
//	private void newOrder() {
//		int customerID = -1;
//		int senderID = -1;
//		int recipientID = -1;
//		int cargoID = -1;
//
//		try{
//			customer = null;
//			customer = (Customers)receiveObject.readObject();
//			sender = (Customers)receiveObject.readObject();
//			recipient = (Customers)receiveObject.readObject();
//			cargo = (Cargos)receiveObject.readObject();
//
//			System.out.println("Klient: "+customer);
//			System.out.println("Nadawca: "+sender);
//			System.out.println("Odbiorca: "+recipient);
//			System.out.println("Towar: "+cargo);
//
//			customerID = customer.getId();
//			cargoID = db.insertCargo(cargo);
//
//			int[] ifExistCustomer;
//			if(sender.getNip() == customer.getNip()){
//				System.out.println("NADAWCA TEN SAM");
//				senderID = customerID;
//			}else{
//				System.out.println("NADAWCA INNY");
//				ifExistCustomer = db.ifExistCustomer(sender);
//				if(ifExistCustomer[0] <= 0){
//					//nie istnieje dodaje klienta ze statusem niezarejestrowanego
//					senderID = db.insertCustomer(sender, 5);
//				}else{
//					//istnieje zwracam id klienta
//					System.out.println("Klient istnieje a jego id to "+ifExistCustomer[1]);
//					senderID = ifExistCustomer[1];
//				}
//			}
//
//			if(recipient.getNip() == customer.getNip()){
//				System.out.println("ODBIORCA TEN SAM");
//				recipientID = customerID;
//			}else{
//				System.out.println("ODBIORCA INNY");
//				ifExistCustomer = db.ifExistCustomer(recipient);
//				if(ifExistCustomer[0] <= 0){
//					//nie istnieje dodaje klienta ze statusem niezarejestrowanego
//					recipientID = db.insertCustomer(recipient, 5);
//				}else{
//					//istnieje zwracam id klienta
//					System.out.println("Klient istnieje a jego id to "+ifExistCustomer[1]);
//					recipientID = ifExistCustomer[1];;
//				}
//			}
//
//
//		}catch(ClassNotFoundException | IOException e){
//			System.err.println("Blad przy odbieraniu obiektow do zlecenia");
//		}
//
//		System.out.println("CUSTOMER"+customerID);
//		System.out.println("SENDER"+senderID);
//		System.out.println("RECIPIENT"+recipientID);
//		System.out.println("CARGO"+cargoID);
//
//
//		if(customerID > 0 & senderID > 0 & recipientID > 0 & cargoID > 0)
//			db.insertOrder(new Orders(customerID, senderID, recipientID, cargoID, 7, new ConvertDate(new Date()).toString(), ""));
//	}
//
//	private void mineOrders() {
//		int customerID = -1;
//
//		try {
//			customerID = Integer.parseInt(receive.readLine());
//			System.out.println("Id Klienta "+customerID);
//		} catch (NumberFormatException | IOException e) {
//			System.err.println("Blad przy odbieraniu id klienta");
//		}
//
//		ArrayList<OrdersClient> clientCustomers = new ArrayList<OrdersClient>();
//		clientCustomers = db.selectClientOrders(customerID);
//		try {
//			sendObject.writeObject(clientCustomers);
//		} catch (IOException e) {
//			System.err.println("Blad podczas wysylania listy zlecen dla danego klienta");
//		}
//
//	}
//
//	private void register() {
//		try{
//			customer = (Customers)receiveObject.readObject();
//		}catch(ClassNotFoundException | IOException e){
//			System.err.println("Blad przy odbieraniu danych do rejestracji");
//		}
//			if(db.ifExistCustomer(customer)[0] > 0){
//				send.println("1");
//			}else{
//				db.insertCustomer(customer, 1);
//				send.println("2");
//			}
//			send.flush();
//	}
//
//	private void login() {
//		try {
//			customer = (Customers) receiveObject.readObject();
//		} catch (ClassNotFoundException | IOException e) {
//			System.err.println("Blad przy odbieraniu obiektu do logowania");
//		}
//
//		try {
//			sendObject.writeObject(db.selectDataLoginCustomer(customer));
//		} catch (IOException e) {
//			System.err.println("Blad przy odsylaniu danych klienta do Klienta");
//		}
//	}
//
//
//
}
