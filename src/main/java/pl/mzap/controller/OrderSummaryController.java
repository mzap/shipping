package pl.mzap.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import lombok.Setter;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.TransportDTO;

import java.net.URL;
import java.util.ResourceBundle;

public class OrderSummaryController implements Initializable {

	@FXML private TextField orderContractorShortNameTextField;
	@FXML private TextField orderContractorNameTextField;
	@FXML private TextField orderContractorVatIdTextField;
	@FXML private TextField orderContractorPhoneTextField;
	@FXML private TextField orderContractorAddressCityTextField;
	@FXML private TextField orderContractorAddressPostalCodeTextField;
	@FXML private TextField orderContractorAddressStreetTextField;
	@FXML private TextField orderContractorAddressBuildingTextField;
	@FXML private TextField orderDateTextField;
	@FXML private TextField orderReferenceNumberTextField;
	@FXML private TextField orderRelationTextField;
	@FXML private TextField orderVehicleCountTextField;
	@FXML private TextField orderCargoCountTextField;
	@FXML private TextField orderTransportCountTextField;
	@FXML private TextField orderPaymentAmountTextField;
	@FXML private TextField orderPaymentTermTextField;

	@Setter private OrderDTO order;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void inflateTemplate() {
		orderDateTextField.setText("###");
		orderReferenceNumberTextField.setText(order.getReferenceNumber());
		orderRelationTextField.setText(order.getRelation());
		orderVehicleCountTextField.setText(String.valueOf(order.getTransports().stream().map(TransportDTO::getVehicle).count()));
		orderCargoCountTextField.setText(String.valueOf(order.getCargos().size()));
		orderTransportCountTextField.setText(String.valueOf(order.getTransports().size()));
		orderPaymentAmountTextField.setText(String.valueOf(order.getPaymentAmount()));
		orderPaymentTermTextField.setText(String.valueOf(order.getPaymentTermInDays()));
		orderContractorNameTextField.setText(order.getContractor().getName());
		orderContractorShortNameTextField.setText(order.getContractor().getShortName());
		orderContractorVatIdTextField.setText(order.getContractor().getVatId());
		orderContractorPhoneTextField.setText(order.getContractor().getPhoneNumber());
		orderContractorAddressCityTextField.setText(order.getContractor().getAddress().getCity());
		orderContractorAddressPostalCodeTextField.setText(order.getContractor().getAddress().getPostalCode());
		orderContractorAddressStreetTextField.setText(order.getContractor().getAddress().getStreet());
		orderContractorAddressBuildingTextField.setText(order.getContractor().getAddress().getBuilding());
	}

	public void initTemplateListener() {
		order.getContractor().nameProperty().bind(orderContractorNameTextField.textProperty());
		order.getContractor().shortNameProperty().bind(orderContractorShortNameTextField.textProperty());
		order.getContractor().vatIdProperty().bind(orderContractorVatIdTextField.textProperty());
		order.getContractor().phoneNumberProperty().bind(orderContractorPhoneTextField.textProperty());
		order.getContractor().getAddress().cityProperty().bind(orderContractorAddressCityTextField.textProperty());
		order.getContractor().getAddress().postalCodeProperty().bind(orderContractorAddressPostalCodeTextField.textProperty());
		order.getContractor().getAddress().streetProperty().bind(orderContractorAddressStreetTextField.textProperty());
		order.getContractor().getAddress().buildingProperty().bind(orderContractorAddressBuildingTextField.textProperty());
		order.referenceNumberProperty().bind(orderReferenceNumberTextField.textProperty());
		order.relationProperty().bind(orderRelationTextField.textProperty());
	}
}
