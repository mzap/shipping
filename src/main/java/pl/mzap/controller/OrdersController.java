package pl.mzap.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import pl.mzap.util.Context;
import pl.mzap.Main;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.status.OrderStatus;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Log4j
public class OrdersController implements Initializable {

	@FXML private TableView<OrderDTO> ordersTable;
	@FXML private TableColumn<OrderDTO, String> contractorColumn;
	@FXML private TableColumn<OrderDTO, String> relationColumn;
	@FXML private TableColumn<OrderDTO, String> referenceNumberColumn;
	@FXML private TableColumn<OrderDTO, String> cargosColumn;
	@FXML private TableColumn<OrderDTO, OrderStatus> statusColumn;

	@FXML private ComboBox<OrderStatus> statusSortFilterComboBox;

	@Setter
	private Main main;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		configureOrderTableColumns();
		inflateOrderTableAndStatusComboBox();
	}

	private void configureOrderTableColumns() {
		contractorColumn.setCellValueFactory(new PropertyValueFactory<>("contractor"));
		relationColumn.setCellValueFactory(new PropertyValueFactory<>("relation"));
		referenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("referenceNumber"));
		cargosColumn.setCellValueFactory(data -> new SimpleStringProperty(String.valueOf(data.getValue().getCargos().size())));
		statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
	}

	private void inflateOrderTableAndStatusComboBox() {
		statusSortFilterComboBox.setItems(OrderStatus.getStatuses());
		ordersTable.setItems(Context.INSTANCE.getOrders());
	}

	public void getOrders(ActionEvent actionEvent) {
		statusSortFilterComboBox.getSelectionModel().clearSelection();
		ordersTable.setItems(Context.INSTANCE.getOrders());
		ordersTable.refresh();
	}

	public void showVehicleView(ActionEvent actionEvent) throws IOException {
		main.showVehiclesWindow();
	}

	public void showNewVehicleView(ActionEvent actionEvent) throws IOException {
		main.showAddVehicleView();
	}

	public void showSingleOrderView(ActionEvent actionEvent) throws IOException {
		OrderDTO selectedOrder = ordersTable.getSelectionModel().getSelectedItem();
		main.showOrderView(selectedOrder);
	}

	public void showCustomersView(ActionEvent actionEvent) throws IOException {
		main.showContractorsWindow();
	}

	public void showNewContractorView(ActionEvent actionEvent) throws IOException {
		main.showAddContractorWindow();
	}

	public void changeStatusSortFilter(ActionEvent actionEvent) {
		ObservableList<OrderDTO> filteredOrders = Context.INSTANCE.getOrders().parallelStream()
				.filter(order -> order.getStatus().equals(statusSortFilterComboBox.getSelectionModel().getSelectedItem()))
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
		ordersTable.setItems(filteredOrders);
	}

	public void clearStatusSortFilters(ActionEvent actionEvent) {
		statusSortFilterComboBox.getSelectionModel().clearSelection();
		ordersTable.setItems(Context.INSTANCE.getOrders());
	}

}
