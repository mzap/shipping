package pl.mzap.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import pl.mzap.util.Context;
import pl.mzap.Main;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.status.ContractorStatus;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

@Log4j
public class ContractorController implements Initializable {

	@FXML public TableView<ContractorDTO> customersTable;
	@FXML public TableColumn<ContractorDTO, String> nameColumn;
	@FXML public TableColumn<ContractorDTO, String> shortNameColumn;
	@FXML public TableColumn<ContractorDTO, String> phoneColumn;
	@FXML public TableColumn<ContractorDTO, String> addressColumn;
	@FXML public TableColumn<ContractorDTO, String> cityColumn;
	@FXML public TableColumn<ContractorDTO, String> streetColumn;
	@FXML public TableColumn<ContractorDTO, String> buildingColumn;
	@FXML public TableColumn<ContractorDTO, String> postalColumn;
	@FXML public TableColumn<ContractorDTO, String> vatIDColumn;
	@FXML public TableColumn<ContractorStatus, String> statusColumn;

	@FXML public ComboBox<ContractorStatus> companyStatusComboBox;

	@Setter private Main main;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		nameColumn.setOnEditCommit(event -> event.getRowValue().setName(event.getNewValue()));

		shortNameColumn.setCellValueFactory(new PropertyValueFactory<>("shortName"));
		shortNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		shortNameColumn.setOnEditCommit(event -> event.getRowValue().setShortName(event.getNewValue()));

		phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
		phoneColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		phoneColumn.setOnEditCommit(event -> event.getRowValue().setPhoneNumber(event.getNewValue()));

		cityColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getCity()));
		cityColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		cityColumn.setOnEditCommit(event -> event.getRowValue().getAddress().setCity(event.getNewValue()));

		streetColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getStreet()));
		streetColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		streetColumn.setOnEditCommit(event -> event.getRowValue().getAddress().setStreet(event.getNewValue()));

		buildingColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getBuilding()));
		buildingColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		buildingColumn.setOnEditCommit(event -> event.getRowValue().getAddress().setBuilding(event.getNewValue()));

		postalColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getPostalCode()));
		postalColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		postalColumn.setOnEditCommit(event -> event.getRowValue().getAddress().setPostalCode(event.getNewValue()));

		vatIDColumn.setCellValueFactory(new PropertyValueFactory<>("vatId"));
		vatIDColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		vatIDColumn.setOnEditCommit(event -> event.getRowValue().setVatId(event.getNewValue()));

		statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));

		inflateContractorTableAndCompanyStatusComboBox();
	}

	private void inflateContractorTableAndCompanyStatusComboBox() {
		customersTable.setItems(Context.INSTANCE.getContractors());
		customersTable.refresh();
		companyStatusComboBox.setItems(ContractorStatus.getStatuses());
	}

	@FXML
	public void addContractorEvent(ActionEvent actionEvent) throws IOException {
		main.showAddContractorWindow();
	}

	@FXML
	public void removeContractorEvent(ActionEvent actionEvent) {
		Context.INSTANCE.removeContractor(getSelectedContractor());
		customersTable.refresh();
	}

	@FXML
	public void changeContractorStatusEvent(ActionEvent actionEvent) {
		getSelectedContractor().setStatus(companyStatusComboBox.getSelectionModel().getSelectedItem());
		customersTable.refresh();
	}

	@FXML
	public void tableRowClicked(MouseEvent mouseEvent) {
		ContractorStatus contractorStatus = getSelectedContractor().getStatus();
		companyStatusComboBox.getSelectionModel().select(contractorStatus);
	}

	private ContractorDTO getSelectedContractor() {
		return Optional.ofNullable(customersTable.getSelectionModel().getSelectedItem())
				.orElseThrow(NullPointerException::new);
	}

}
