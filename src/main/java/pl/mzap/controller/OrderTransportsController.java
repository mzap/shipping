package pl.mzap.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import pl.mzap.domain.dto.*;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class OrderTransportsController implements Initializable {

	@Getter
	@FXML private TableView<TransportDTO> transportsTable;
	@FXML private TableColumn<TransportDTO, Long> transportNoColumn;
	@FXML private TableColumn<TransportDTO, String> transportReferenceColumn;
	@FXML private TableColumn<TransportDTO, String> transportActionColumn;
	@FXML private TableColumn<TransportDTO, VehicleDTO> transportVehiclePlateColumn;
	@FXML private TableColumn<TransportDTO, CargoDTO> transportCargoNameColumn;
	@FXML private TableColumn<TransportDTO, LocalDateTime> transportWhenColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressNameColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressCityColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressStreetColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressBuildingColumn;
	@FXML private TableColumn<TransportDTO, String> transportAddressPostalColumn;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		transportNoColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
		transportReferenceColumn.setCellValueFactory(new PropertyValueFactory<>("referenceNumber"));
		transportActionColumn.setCellValueFactory(new PropertyValueFactory<>("transportAction"));
		transportVehiclePlateColumn.setCellValueFactory(new PropertyValueFactory<>("vehicle"));
		transportCargoNameColumn.setCellValueFactory(new PropertyValueFactory<>("cargo"));
		transportWhenColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
		transportAddressColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
		transportAddressNameColumn.setCellValueFactory(new PropertyValueFactory<>("companyName"));
		transportAddressCityColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getCity()));
		transportAddressStreetColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getStreet()));
		transportAddressBuildingColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getBuilding()));
		transportAddressPostalColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getAddress().getPostalCode()));
	}

	public void inflateTable(OrderDTO order) {
		transportsTable.setItems(order.getTransports());
	}
}
