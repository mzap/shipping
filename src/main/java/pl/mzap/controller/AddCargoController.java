package pl.mzap.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import pl.mzap.domain.dto.CargoDTO;
import pl.mzap.domain.dto.OrderDTO;

import java.net.URL;
import java.util.ResourceBundle;

public class AddCargoController implements Initializable {

	@FXML public TextField cargoName;
	@FXML public TextArea cargoDescription;
	@FXML public TextField cargoNumber;

	@Setter private Stage dialogStage;

	@Setter private OrderDTO order;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {

	}

	@FXML
	public void addCargo(ActionEvent actionEvent) {
		CargoDTO cargo = CargoDTO.builder()
				.name(new SimpleStringProperty(cargoName.getText()))
				.description(new SimpleStringProperty(cargoDescription.getText()))
				.number(new SimpleStringProperty(cargoNumber.getText()))
				.build();
		order.getCargos().add(cargo);
		dialogStage.close();
	}

}
