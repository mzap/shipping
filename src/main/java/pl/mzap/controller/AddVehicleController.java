package pl.mzap.controller;

import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import pl.mzap.domain.dto.VehicleLocationHistoryDTO;
import pl.mzap.util.Context;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.status.VehicleStatus;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class AddVehicleController implements Initializable {

	@FXML private TextField vehiclePlateTextField;
	@FXML private TextField vehicleDriverNameTextField;
	@FXML private ComboBox<VehicleKind> vehicleKindComboBox;
	@FXML private ComboBox<VehicleStatus> vehicleStatusComboBox;

	@Setter private Stage dialogStage;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		vehicleKindComboBox.setItems(VehicleKind.getVehicleKinds());
		vehicleStatusComboBox.setItems(VehicleStatus.getStatuses());
	}

	@FXML
	public void addVehicleAction(ActionEvent actionEvent) {
		VehicleDTO vehicle = VehicleDTO.builder()
				.plate(new SimpleStringProperty(vehiclePlateTextField.getText()))
				.driver(new SimpleStringProperty(vehicleDriverNameTextField.getText()))
				.vehicleKind(new SimpleObjectProperty<>(vehicleKindComboBox.getSelectionModel().getSelectedItem()))
				.status(new SimpleMapProperty<>(FXCollections.observableHashMap()))
				.build();
		vehicle.addStatus(vehicleStatusComboBox.getSelectionModel().getSelectedItem());
		Context.INSTANCE.addVehicle(vehicle);
		dialogStage.close();
	}

}
