package pl.mzap.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import pl.mzap.Main;
import pl.mzap.domain.dto.CargoDTO;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.TransportDTO;
import pl.mzap.domain.status.ContractorStatus;
import pl.mzap.domain.status.OrderStatus;
import pl.mzap.services.OrderTabInflater;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

@Log4j
public class OrderController extends OrderTabInflater implements Initializable {

	@Getter
	@FXML public AnchorPane summaryTabPane;
	@Getter
	@FXML public AnchorPane vehiclesTabPane;
	@Getter
	@FXML public AnchorPane cargosTabPane;
	@Getter
	@FXML public AnchorPane transportsTabPane;
	@Getter
	@FXML public AnchorPane historyTabPane;

	@FXML private ComboBox<OrderStatus> orderStatusComboBox;
	@FXML private Label orderStatusDate;

	@FXML public TabPane tabPaneContener;
	@FXML private Tab summaryTab;
	@FXML private Tab vehiclesTab;
	@FXML private Tab cargosTab;
	@FXML private Tab transportsTab;
	@FXML private Tab historyTab;

	@Setter
	private Main main;
	@Setter
	private OrderDTO order;

	@Setter private OrderSummaryController summaryController;
	@Setter private OrderVehiclesController vehiclesController;
	@Setter private OrderCargosController cargosController;
	@Setter private OrderTransportsController transportsController;
	@Setter private OrderHistoryController historyController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void inflateOrderStatusComboBox() {
		orderStatusComboBox.setItems(OrderStatus.getStatuses());
		Optional.ofNullable(order)
				.ifPresent(order -> orderStatusComboBox.getSelectionModel().select(order.getStatus()));
	}

	public void inflateTabPanesTemplate() {
		inflateOrderSummaryGrid(summaryController, order);
		inflateOrderVehiclesTable(vehiclesController, order);
		inflateOrderCargosTable(cargosController, order);
		inflateOrderTransportsTable(transportsController, order);
		inflateOrderHistoryList(historyController, order);
	}

	public void changeOrderStatus(ActionEvent actionEvent) throws IOException {
		OrderStatus newSelectedOrderStatus = orderStatusComboBox.getSelectionModel().getSelectedItem();
		order.setStatus(newSelectedOrderStatus);
	}

	public void addCargoAction(ActionEvent actionEvent) throws IOException {
		tabPaneContener.getSelectionModel().select(cargosTab);
		main.showAddCargoView(order);
	}

	public void removeCargoAction(ActionEvent actionEvent) {
		CargoDTO cargo = cargosController.getCargosTable().getSelectionModel().getSelectedItem();
		order.getCargos().remove(cargo);
	}

	public void addTransportAction(ActionEvent actionEvent) throws IOException {
		tabPaneContener.getSelectionModel().select(transportsTab);
		main.showAddTransportView(order);
	}

	public void removeTransportAction(ActionEvent actionEvent) {
		TransportDTO transport = transportsController.getTransportsTable().getSelectionModel().getSelectedItem();
		order.getTransports().remove(transport);
	}
}
