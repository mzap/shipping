package pl.mzap.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import pl.mzap.util.Context;
import pl.mzap.domain.dto.AddressDTO;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.status.ContractorStatus;

import java.net.URL;
import java.util.ResourceBundle;

public class AddContractorController implements Initializable {

	@FXML private TextField nameTextField;
	@FXML private TextField shortNameTextField;
	@FXML private TextField phoneNumberTextField;
	@FXML private TextField vatIdTextField;
	@FXML private TextField addressCityTextField;
	@FXML private TextField addressPostalCodeTextField;
	@FXML private TextField addressStreetTextField;
	@FXML private TextField addressNumberTextField;
	@FXML private ComboBox<ContractorStatus> statusComboBox;

	@Setter private Stage dialogStage;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		statusComboBox.setItems(ContractorStatus.getStatuses());
	}

	@FXML
	public void addContractorAction(ActionEvent actionEvent) {
		ContractorDTO contractor = ContractorDTO.builder()
				.name(new SimpleStringProperty(nameTextField.getText()))
				.shortName(new SimpleStringProperty(shortNameTextField.getText()))
				.phoneNumber(new SimpleStringProperty(phoneNumberTextField.getText()))
				.vatId(new SimpleStringProperty(vatIdTextField.getText()))
				.address(new SimpleObjectProperty<>(AddressDTO.builder()
						.city(new SimpleStringProperty(addressCityTextField.getText()))
						.postalCode(new SimpleStringProperty(addressPostalCodeTextField.getText()))
						.street(new SimpleStringProperty(addressStreetTextField.getText()))
						.building(new SimpleStringProperty(addressNumberTextField.getText()))
						.build())
				)
				.status(new SimpleObjectProperty<>(statusComboBox.getSelectionModel().getSelectedItem()))
				.build();
		Context.INSTANCE.addContractors(contractor);
		dialogStage.close();
	}
}
