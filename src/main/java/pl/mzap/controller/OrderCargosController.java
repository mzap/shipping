package pl.mzap.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import pl.mzap.domain.dto.CargoDTO;
import pl.mzap.domain.dto.OrderDTO;

import java.net.URL;
import java.util.ResourceBundle;

public class OrderCargosController implements Initializable {

	@Getter
	@FXML private TableView<CargoDTO> cargosTable;
	@FXML private TableColumn<CargoDTO, Long> cargoNoColumn;
	@FXML private TableColumn<CargoDTO, String> cargoNameColumn;
	@FXML private TableColumn<CargoDTO, String> cargoNumberColumn;
	@FXML private TableColumn<CargoDTO, String> cargoDescriptionColumn;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		cargoNoColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
		cargoNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		cargoNumberColumn.setCellValueFactory(new PropertyValueFactory<>("number"));
		cargoDescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
	}

	public void inflateTable(OrderDTO order) {
		cargosTable.setItems(order.getCargos());
		cargosTable.refresh();
	}
}
