package pl.mzap.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.TransportDTO;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.status.VehicleStatus;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class OrderVehiclesController implements Initializable {

	@FXML private TableView<VehicleDTO> vehiclesTable;
	@FXML private TableColumn<VehicleDTO, Long> vehicleNoColumn;
	@FXML private TableColumn<VehicleDTO, String> vehiclePlateColumn;
	@FXML private TableColumn<VehicleDTO, String> vehicleDriverColumn;
	@FXML private TableColumn<VehicleDTO, VehicleKind> vehicleKindColumn;
	@FXML private TableColumn<VehicleDTO, VehicleStatus> vehicleStatusColumn;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		vehicleNoColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
		vehiclePlateColumn.setCellValueFactory(new PropertyValueFactory<>("plate"));
		vehicleDriverColumn.setCellValueFactory(new PropertyValueFactory<>("driver"));
		vehicleKindColumn.setCellValueFactory(new PropertyValueFactory<>("vehicleKind"));
		vehicleStatusColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue().getStatus().values().iterator().next().getStatus()));
	}

	public void inflateTable(OrderDTO order) {
		vehiclesTable.setItems(order.getTransports().stream()
				.map(TransportDTO::getVehicle)
				.distinct()
				.collect(Collectors.toCollection(FXCollections::observableArrayList)));
		vehiclesTable.refresh();
	}

}
