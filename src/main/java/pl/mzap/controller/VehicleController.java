package pl.mzap.controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.service.directions.DirectionsService;
import com.lynden.gmapsfx.service.geocoding.GeocoderStatus;
import com.lynden.gmapsfx.service.geocoding.GeocodingResult;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import pl.mzap.Main;
import pl.mzap.domain.dto.ContractorDTO;
import pl.mzap.domain.dto.OrderDTO;
import pl.mzap.domain.dto.VehicleDTO;
import pl.mzap.domain.kind.VehicleKind;
import pl.mzap.domain.status.VehicleStatus;
import pl.mzap.util.Constant;
import pl.mzap.util.Context;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static pl.mzap.util.Constant.GOOGLE_MAPS_API_KEY;

@Log4j
public class VehicleController implements Initializable, MapComponentInitializedListener {

	@FXML private TableView<VehicleDTO> vehicleTableView;
	@FXML private TableColumn<VehicleDTO, String> vehiclePlateColumn;
	@FXML private TableColumn<VehicleDTO, String> vehicleDriverColumn;
	@FXML private TableColumn<VehicleDTO, VehicleKind> vehicleKindColumn;
	@FXML private TableView<OrderDTO> assignedOrdersTableView;
	@FXML private TableColumn<OrderDTO, ContractorDTO> assignedOrderContractorColumn;
	@FXML private TableColumn<OrderDTO, String> assignedOrderReferenceNumberColumn;

	@FXML private ComboBox<VehicleStatus> vehicleStatusComboBox;
	@FXML private ComboBox<VehicleKind> vehicleKindComboBox;

	@FXML private ListView<String> vehicleHistoryStatusListView;

	@FXML private GoogleMapView mapView;

	@Setter private Main main;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mapView.addMapInializedListener(this);
//        mapView.setKey(GOOGLE_MAPS_API_KEY);//todo: use proper google key
		configureVehicleTableView();
		configureAssignedOrdersTableView();
		inflateVehiclesTable();
		inflateComboBoxes();
	}

	private void configureVehicleTableView() {
		vehiclePlateColumn.setCellValueFactory(new PropertyValueFactory<>("plate"));
		vehiclePlateColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		vehiclePlateColumn.setOnEditCommit(event -> event.getRowValue().setPlate(event.getNewValue()));

		vehicleDriverColumn.setCellValueFactory(new PropertyValueFactory<>("driver"));
		vehicleDriverColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		vehicleDriverColumn.setOnEditCommit(event -> event.getRowValue().setDriver(event.getNewValue()));

		vehicleKindColumn.setCellValueFactory(new PropertyValueFactory<>("vehicleKind"));
	}

	private void configureAssignedOrdersTableView() {
		assignedOrderContractorColumn.setCellValueFactory(new PropertyValueFactory<>("contractor"));
		assignedOrderReferenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("referenceNumber"));
	}

	private void inflateVehiclesTable() {
		vehicleTableView.setItems(Context.INSTANCE.getVehicles());
	}

	private void inflateComboBoxes() {
		vehicleStatusComboBox.setItems(VehicleStatus.getStatuses());
		vehicleKindComboBox.setItems(VehicleKind.getVehicleKinds());
	}

	private void inflateOrderTableByVehicle(VehicleDTO vehicle) {
		ObservableList<OrderDTO> orders = Context.INSTANCE.getOrders();
		ObservableList<OrderDTO> vehicleOrders = orders.stream()
				.filter(
						order -> order.getTransports().stream()
								.anyMatch(transport -> transport.getVehicle().getPlate().equals(vehicle.getPlate()))
				).collect(Collectors.toCollection(FXCollections::observableArrayList));
		assignedOrdersTableView.setItems(vehicleOrders);
	}

	private void inflateVehicleHistoryStatus(VehicleDTO vehicle) {
		ObservableList<String> historyLocation = FXCollections.observableArrayList();
		vehicle.getStatus().values().stream()
				.forEach(data -> historyLocation.add(data.getStatus().getName() + Constant.ARROW + data.getLocation().getCity() + Constant.SPACE + data.getLocation().getStreet()));
		vehicleHistoryStatusListView.setItems(historyLocation);
	}

	@Override
	public void mapInitialized() {
		GeocodingService geocodingService = new GeocodingService();
		MapOptions mapOptions = new MapOptions();

		mapOptions.center(new LatLong(52.0671511, 19.7452595))
				.mapType(MapTypeIdEnum.ROADMAP)
				.overviewMapControl(false)
				.panControl(false)
				.rotateControl(false)
				.scaleControl(false)
				.streetViewControl(false)
				.zoomControl(false)
				.zoom(6);

		GoogleMap map = mapView.createMap(mapOptions);
		DirectionsService directionsService = new DirectionsService();
	    DirectionsPane directionsPane = mapView.getDirec();
	}

	@FXML
	public void vehicleSelectionAction(MouseEvent mouseEvent) {
		inflateOrderTableByVehicle(getSelectedVehicle());
		inflateVehicleHistoryStatus(getSelectedVehicle());
		setComboBoxValues();
	}

	private void setComboBoxValues() {
		vehicleKindComboBox.getSelectionModel().select(getSelectedVehicle().getVehicleKind());
		vehicleStatusComboBox.getSelectionModel().select(getSelectedVehicle().getStatus().values().iterator().next().getStatus());
	}

	private VehicleDTO getSelectedVehicle() {
		return vehicleTableView.getSelectionModel().getSelectedItem();
	}

	@FXML
	public void addVehicleAction(ActionEvent actionEvent) throws IOException {
		main.showAddVehicleView();
	}

	@FXML
	public void removeVehicleAction(ActionEvent actionEvent) {
		Context.INSTANCE.removeVehicle(getSelectedVehicle());
	}

	@FXML
	public void changeVehicleKindEvent(ActionEvent actionEvent) {
		getSelectedVehicle().setVehicleKind(vehicleKindComboBox.getSelectionModel().getSelectedItem());
		vehicleTableView.refresh();
	}

	@FXML
	public void changeVehicleStatusEvent(ActionEvent actionEvent) {
		getSelectedVehicle().addStatus(vehicleStatusComboBox.getSelectionModel().getSelectedItem());
		vehicleTableView.refresh();
	}
}
