package pl.mzap.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import pl.mzap.util.Constant;
import pl.mzap.util.Context;
import pl.mzap.domain.dto.*;
import pl.mzap.domain.kind.TransportAction;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class AddTransportController implements Initializable {

	@FXML private Button addTransportButton;
	@FXML private ComboBox<TransportAction> transportActionComboBox;
	@FXML private DatePicker transportTermDatePicker;
	@FXML private TextField transportTermHourTimeTextField;
	@FXML private TextField transportTermMinuteTimeTextField;
	@FXML private TextField locationDescriptionTextField;
	@FXML private TextField addressCityTextField;
	@FXML private TextField addressPostalCodeTextField;
	@FXML private TextField addressStreetTextField;
	@FXML private TextField addressNumberTextField;
	@FXML private ComboBox<CargoDTO> transportCargoComboBox;
	@FXML private ComboBox<VehicleDTO> transportVehicleComboBox;

	@Setter private OrderDTO order;
	@Setter private Stage dialogStage;

	private String referenceNumber = generateReferenceNumber();

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		transportActionComboBox.setItems(TransportAction.getTransportActions());
		transportVehicleComboBox.setItems(Context.INSTANCE.getVehicles());
		addTransportButton.setText(addTransportButton.getText() + Constant.SPACE + referenceNumber);
	}

	public void inflateCargoComboBox() {
		transportCargoComboBox.setItems(order.getCargos());
	}

	private String generateReferenceNumber() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yy-SSS"));
	}

	private LocalDateTime getPickedDateTime() {
		LocalTime time = LocalTime.of(Integer.parseInt(transportTermHourTimeTextField.getText()), Integer.parseInt(transportTermMinuteTimeTextField.getText()));
		LocalDate date = transportTermDatePicker.getValue();
		return LocalDateTime.of(date, time);
	}

	@FXML
	public void addTransportAction(ActionEvent actionEvent) {
		TransportDTO transport = TransportDTO.builder()
				.transportAction(new SimpleObjectProperty<>(transportActionComboBox.getSelectionModel().getSelectedItem()))
				.date(new SimpleObjectProperty<>(getPickedDateTime()))
				.companyName(new SimpleStringProperty(locationDescriptionTextField.getText()))
				.address(new SimpleObjectProperty<>(AddressDTO.builder()
						.city(new SimpleStringProperty(addressCityTextField.getText()))
						.postalCode(new SimpleStringProperty(addressPostalCodeTextField.getText()))
						.street(new SimpleStringProperty(addressStreetTextField.getText()))
						.building(new SimpleStringProperty(addressNumberTextField.getText()))
						.build()
				))
				.cargo(new SimpleObjectProperty<>(transportCargoComboBox.getSelectionModel().getSelectedItem()))
				.vehicle(new SimpleObjectProperty<>(transportVehicleComboBox.getSelectionModel().getSelectedItem()))
				.referenceNumber(new SimpleStringProperty(referenceNumber))
				.build();
		order.getTransports().add(transport);
		dialogStage.close();
	}


}
